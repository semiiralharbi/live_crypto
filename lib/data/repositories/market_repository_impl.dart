import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../domain/data_source/market_data_source.dart';
import '../../domain/entities/asset_details_entity.dart';
import '../../domain/entities/asset_icon_entity.dart';
import '../../domain/entities/symbol_entity.dart';
import '../../domain/entities/trades_entity.dart';
import '../../domain/repositories/market_repository.dart';
import '../../domain/utils/failure.dart';

@Injectable(as: MarketRepository)
class MarketRepositoryImpl implements MarketRepository {
  const MarketRepositoryImpl(this._dataSource);

  final MarketDataSource _dataSource;

  @override
  Future<Either<Failure, List<AssetDetailsEntity>>> getAssetDetails(
    String assetId,
  ) async {
    try {
      final details = await _dataSource.getAssetDetails(assetId);
      return Right(
        details.map((dto) => AssetDetailsEntity.fromDto(dto)).toList(),
      );
    } catch (_) {
      return const Left(Failure());
    }
  }

  @override
  Future<Either<Failure, List<AssetIconEntity>>> getAssetIcons(int size) async {
    try {
      final icons = await _dataSource.getAssetIcon(size);
      return Right(icons.map((dto) => AssetIconEntity.fromDto(dto)).toList());
    } catch (_) {
      return const Left(Failure());
    }
  }

  @override
  Future<Either<Failure, List<SymbolEntity>>> getSymbolDetails(
    String symbolId,
  ) async {
    try {
      final symbols = await _dataSource.getSymbolDetails(symbolId);
      return Right(symbols.map((dto) => SymbolEntity.fromDto(dto)).toList());
    } catch (_) {
      return const Left(Failure());
    }
  }

  @override
  Stream<TradesEntity> getTrades(String assetID) {
    final result = _dataSource.getTrades(assetID);
    return result.map((event) => TradesEntity.fromDto(event));
  }
}
