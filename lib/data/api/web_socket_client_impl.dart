import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../environment_config.dart';
import 'web_socket_client.dart';

@Injectable(as: WebSocketClient)
class WebSocketClientImpl implements WebSocketClient {
  const WebSocketClientImpl(this._channel);

  final WebSocketChannel _channel;

  @override
  Stream get message => _channel.stream;

  @override
  void sendTradeHello(String filterAssetId) {
    final message = {
      "type": "hello",
      "apikey": EnvironmentConfig.cryptoKey,
      "heartbeat": false,
      "subscribe_data_type": ["trade"],
      "subscribe_filter_asset_id": [filterAssetId],
      "subscribe_filter_symbol_id": [
        "BINANCE_",
      ],
    };
    _channel.sink.add(jsonEncode(message));
  }

  @override
  void get close => _channel.sink.close();
}
