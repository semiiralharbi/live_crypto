abstract class WebSocketClient {
  Stream get message;

  void sendTradeHello(String filterAssetId);

  void get close;
}
