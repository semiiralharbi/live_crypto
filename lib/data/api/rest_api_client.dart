import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';

import '../dto/asset_details_dto.dart';
import '../dto/asset_icon_dto.dart';
import '../dto/symbol_dto.dart';

part 'rest_api_client.g.dart';

@injectable
@RestApi()
abstract class RestApiClient {
  @factoryMethod
  factory RestApiClient(Dio dio) => _RestApiClient(dio);

  @GET('assets/{id}')
  Future<List<AssetDetailsDto>> getAssetsDetails(@Path('id') String assetId);

  @GET('assets/icons/{size}')
  Future<List<AssetIconDto>> getAssetsIcons(@Path('size') int size);

  @GET('symbols')
  Future<List<SymbolDto>> getSymbolDetails(
    @Query('filter_symbol_id') String symbolId,
  );
}
