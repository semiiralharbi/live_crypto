import 'package:freezed_annotation/freezed_annotation.dart';

part 'symbol_dto.freezed.dart';
part 'symbol_dto.g.dart';

@freezed
class SymbolDto with _$SymbolDto {
  const factory SymbolDto({
    required String exchangeId,
    required double assetIdBase,
    required double assetIdQuote,
    required String volume1hrs,
    required String volume1hrsUsd,
    required String price,
    required String dataStart,
  }) = _SymbolDto;

  factory SymbolDto.fromJson(Map<String, dynamic> json) =>
      _$SymbolDtoFromJson(json);
}
