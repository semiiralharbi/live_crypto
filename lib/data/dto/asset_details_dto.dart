import 'package:freezed_annotation/freezed_annotation.dart';

part 'asset_details_dto.freezed.dart';

part 'asset_details_dto.g.dart';

@freezed
class AssetDetailsDto with _$AssetDetailsDto {
  const factory AssetDetailsDto({
    required String assetId,
    required String name,
    required double volume1hrsUsd,
    required double volume1dayUsd,
    required double volume1mthUsd,
    required double priceUsd,
    required String dataStart,
    required String dataEnd,
  }) = _AssetDetailsDto;

  factory AssetDetailsDto.fromJson(Map<String, dynamic> json) =>
      _$AssetDetailsDtoFromJson(json);
}
