import 'package:freezed_annotation/freezed_annotation.dart';

part 'asset_icon_dto.freezed.dart';

@freezed
class AssetIconDto with _$AssetIconDto {
  const factory AssetIconDto({
    required String assetId,
    required String? iconUrl,
  }) = _AssetIconDto;

  factory AssetIconDto.fromJson(Map<String, dynamic> json) {
    final iconUrl = json.values.firstWhere(
      (element) => element is String && element.startsWith('http'),
      orElse: () => null,
    );
    return AssetIconDto(
      assetId: json['asset_id'],
      iconUrl: iconUrl,
    );
  }
}
