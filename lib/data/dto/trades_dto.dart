import 'package:freezed_annotation/freezed_annotation.dart';

part 'trades_dto.freezed.dart';
part 'trades_dto.g.dart';

@freezed
class TradesDto with _$TradesDto {
  const factory TradesDto({
    required String timeExchange,
    required double price,
    required double size,
    required String takerSide,
    required String symbolId,
  }) = _TradesDto;

  factory TradesDto.fromJson(Map<String, dynamic> json) =>
      _$TradesDtoFromJson(json);
}
