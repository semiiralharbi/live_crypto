import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import '../../domain/data_source/market_data_source.dart';
import '../../domain/utils/exception.dart';
import '../api/rest_api_client.dart';
import '../api/web_socket_client.dart';
import '../dto/asset_details_dto.dart';
import '../dto/asset_icon_dto.dart';
import '../dto/symbol_dto.dart';
import '../dto/trades_dto.dart';

@Injectable(as: MarketDataSource)
class MarketDataSourceImpl implements MarketDataSource {
  const MarketDataSourceImpl(this._restApiClient, this._socketClient);

  final RestApiClient _restApiClient;
  final WebSocketClient _socketClient;

  @override
  Future<List<AssetDetailsDto>> getAssetDetails(String assetID) async {
    try {
      return await _restApiClient.getAssetsDetails(assetID);
    } on DioError catch (_) {
      throw ApiException();
    }
  }

  @override
  Future<List<AssetIconDto>> getAssetIcon(int size) async {
    try {
      return await _restApiClient.getAssetsIcons(size);
    } on DioError catch (_) {
      throw ApiException();
    }
  }

  @override
  Future<List<SymbolDto>> getSymbolDetails(String symbolId) async {
    try {
      return await _restApiClient.getSymbolDetails(symbolId);
    } on DioError catch (_) {
      throw ApiException();
    }
  }

  @override
  Stream<TradesDto> getTrades(String assetID) {
    final controller = StreamController<TradesDto>();

    _socketClient.sendTradeHello(assetID);
    _socketClient.message.listen((event) {
      final decodedData = jsonDecode(event);
      final dto = TradesDto.fromJson(decodedData);
      controller.add(dto);
    });

    return controller.stream;
  }
}
