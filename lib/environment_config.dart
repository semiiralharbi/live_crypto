class EnvironmentConfig {
  static const cryptoWS = String.fromEnvironment('DEFINE_CRYPTO_WS');
  static const cryptoUrl = String.fromEnvironment('DEFINE_CRYPTO_URL');
  static const cryptoKey = String.fromEnvironment('DEFINE_CRYPTO_KEY');
}
