import 'package:dartz/dartz.dart';

import '../entities/asset_details_entity.dart';
import '../entities/asset_icon_entity.dart';
import '../entities/symbol_entity.dart';
import '../entities/trades_entity.dart';
import '../utils/failure.dart';

abstract class MarketRepository {
  Future<Either<Failure, List<AssetDetailsEntity>>> getAssetDetails(String assetId);

  Future<Either<Failure, List<AssetIconEntity>>> getAssetIcons(int size);

  Future<Either<Failure, List<SymbolEntity>>> getSymbolDetails(String symbolId);

  Stream<TradesEntity> getTrades(String assetID);
}
