import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../entities/symbol_entity.dart';
import '../repositories/market_repository.dart';
import '../utils/failure.dart';
import '../utils/use_case.dart';

@injectable
class GetSymbolDetailsUseCase implements UseCase<List<SymbolEntity>, String> {
  const GetSymbolDetailsUseCase(this._repository);

  final MarketRepository _repository;

  @override
  Future<Either<Failure, List<SymbolEntity>>> call(String symbolId) async {
    final result = await _repository.getSymbolDetails(symbolId);
    return result.fold(
      (l) => Left(l),
      (r) => Right(r),
    );
  }
}
