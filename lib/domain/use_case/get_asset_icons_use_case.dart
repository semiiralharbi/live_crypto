import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../entities/asset_icon_entity.dart';
import '../repositories/market_repository.dart';
import '../utils/failure.dart';
import '../utils/use_case.dart';

@injectable
class GetAssetIconsUseCase
    implements UseCase<List<AssetIconEntity>, int> {
  const GetAssetIconsUseCase(this._repository);

  final MarketRepository _repository;

  @override
  Future<Either<Failure, List<AssetIconEntity>>> call(int size) async {
    final result = await _repository.getAssetIcons(size);
    return result.fold(
          (l) => Left(l),
          (r) => Right(r),
    );
  }
}
