import 'dart:async';

import 'package:injectable/injectable.dart';

import '../entities/trades_entity.dart';
import '../repositories/market_repository.dart';
import '../utils/use_case.dart';

@injectable
class TradesProcessorUseCase
    implements StreamUseCase<List<TradesEntity>, String> {
  TradesProcessorUseCase(this._repository);

  final MarketRepository _repository;
  final uniqueTrades = <TradesEntity>{};

  @override
  Stream<List<TradesEntity>> call(String assetID) {
    final result = _repository.getTrades(assetID);
    final transformer =
        StreamTransformer<TradesEntity, List<TradesEntity>>.fromHandlers(
      handleData: (trade, sink) {
        final uniqueTrades = _getUniqueTrades(trade);
        sink.add(uniqueTrades);
      },
    );
    return result.transform(transformer);
  }

  List<TradesEntity> _getUniqueTrades(TradesEntity trade) {
    final exitingIndex = uniqueTrades
        .toList()
        .indexWhere((element) => element.symbolId == trade.symbolId);
    if (exitingIndex == -1) {
      uniqueTrades.add(trade);
    } else {
      final updatedList = uniqueTrades.toList();
      updatedList[exitingIndex] = trade;
      uniqueTrades.clear();
      uniqueTrades.addAll(updatedList);
    }
    return uniqueTrades.toList();
  }
}
