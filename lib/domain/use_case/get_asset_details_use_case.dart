import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../entities/asset_details_entity.dart';
import '../repositories/market_repository.dart';
import '../utils/failure.dart';
import '../utils/use_case.dart';

@injectable
class GetAssetDetailsUseCase
    implements UseCase<List<AssetDetailsEntity>, String> {
  const GetAssetDetailsUseCase(this._repository);

  final MarketRepository _repository;

  @override
  Future<Either<Failure, List<AssetDetailsEntity>>> call(String assetId) async {
    final result = await _repository.getAssetDetails(assetId);
    return result.fold(
      (l) => Left(l),
      (r) => Right(r),
    );
  }
}
