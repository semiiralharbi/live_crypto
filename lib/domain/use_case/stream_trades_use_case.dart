import 'package:injectable/injectable.dart';

import '../entities/trades_entity.dart';
import '../repositories/market_repository.dart';
import '../utils/use_case.dart';

@injectable
class StreamTradesUseCase implements StreamUseCase<TradesEntity, String> {
  const StreamTradesUseCase(this._repository);

  final MarketRepository _repository;

  @override
  Stream<TradesEntity> call(String assetID) {
    return _repository.getTrades(assetID);
  }
}
