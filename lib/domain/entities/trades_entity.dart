import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/dto/trades_dto.dart';

part 'trades_entity.freezed.dart';

@freezed
class TradesEntity with _$TradesEntity {
  const factory TradesEntity({
    required String timeExchange,
    required double price,
    required double size,
    required String takerSide,
    required String symbolId,
  }) = _TradesEntity;

  factory TradesEntity.fromDto(TradesDto dto) => TradesEntity(
        timeExchange: dto.timeExchange,
        price: dto.price,
        size: dto.size,
        takerSide: dto.takerSide,
        symbolId: dto.symbolId,
      );
}
