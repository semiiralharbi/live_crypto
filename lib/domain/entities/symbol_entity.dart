import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/dto/symbol_dto.dart';

part 'symbol_entity.freezed.dart';

@freezed
class SymbolEntity with _$SymbolEntity {
  const factory SymbolEntity({
    required String exchangeId,
    required double assetIdBase,
    required double assetIdQuote,
    required String volume1hrs,
    required String volume1hrsUsd,
    required String price,
    required String dataStart,
  }) = _SymbolEntity;

  factory SymbolEntity.fromDto(SymbolDto dto) => SymbolEntity(
        exchangeId: dto.exchangeId,
        assetIdBase: dto.assetIdBase,
        assetIdQuote: dto.assetIdQuote,
        volume1hrs: dto.volume1hrs,
        volume1hrsUsd: dto.volume1hrsUsd,
        price: dto.price,
        dataStart: dto.dataStart,
      );
}
