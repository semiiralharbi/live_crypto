import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/dto/asset_icon_dto.dart';

part 'asset_icon_entity.freezed.dart';

@freezed
class AssetIconEntity with _$AssetIconEntity {
  const factory AssetIconEntity({
    required String assetId,
    required String? iconUrl,
  }) = _AssetIconEntity;

  factory AssetIconEntity.fromDto(AssetIconDto dto) => AssetIconEntity(
        assetId: dto.assetId,
        iconUrl: dto.iconUrl,
      );
}
