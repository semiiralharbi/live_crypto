import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/dto/asset_details_dto.dart';

part 'asset_details_entity.freezed.dart';

@freezed
class AssetDetailsEntity with _$AssetDetailsEntity {
  const factory AssetDetailsEntity({
    required String assetId,
    required String name,
    required double volume1hrsUsd,
    required double volume1dayUsd,
    required double volume1mthUsd,
    required double priceUsd,
    required String dataStart,
    required String dataEnd,
  }) = _AssetDetailsEntity;

  factory AssetDetailsEntity.fromDto(AssetDetailsDto dto) => AssetDetailsEntity(
        assetId: dto.assetId,
        name: dto.name,
        volume1hrsUsd: dto.volume1hrsUsd,
        volume1dayUsd: dto.volume1dayUsd,
        volume1mthUsd: dto.volume1mthUsd,
        priceUsd: dto.priceUsd,
        dataStart: dto.dataStart,
        dataEnd: dto.dataEnd,
      );
}
