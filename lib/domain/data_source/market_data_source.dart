import '../../data/dto/asset_details_dto.dart';
import '../../data/dto/asset_icon_dto.dart';
import '../../data/dto/symbol_dto.dart';
import '../../data/dto/trades_dto.dart';

abstract class MarketDataSource {
  Future<List<AssetDetailsDto>> getAssetDetails(String assetId);

  Future<List<AssetIconDto>> getAssetIcon(int size);

  Future<List<SymbolDto>> getSymbolDetails(String symbolId);

  Stream<TradesDto> getTrades(String assetID);


}
