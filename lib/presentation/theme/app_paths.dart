class AppPaths {
  const AppPaths._();

  static const pngPath = 'lib/presentation/assets/images/png';
  static const svgPath = 'lib/presentation/assets/images/svg';

  ///ADD YOU PATHS HERE EXAMPLE:
  ///static const logo = '$pngPath/logo.png';
  }