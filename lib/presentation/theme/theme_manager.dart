import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:injectable/injectable.dart';

import 'app_colors.dart';
import 'app_dimensions.dart';

@singleton
class ThemeManager {
  final _themeData = ThemeData(
    brightness: Brightness.dark,
    elevatedButtonTheme: const ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStatePropertyAll<Color>(
          AppColors.shamrock,
        ),
        foregroundColor: MaterialStatePropertyAll<Color>(
          AppColors.grayChanteau,
        ),
      ),
    ),
    appBarTheme: const AppBarTheme(
      color: Colors.transparent,
      elevation: 0,
      foregroundColor: AppColors.shamrock,
      iconTheme: IconThemeData(
        size: AppDimensions.d28,
        shadows: [
          Shadow(
            color: AppColors.grayChanteau,
            blurRadius: AppDimensions.d1,
          ),
        ],
      ),
    ),
    textTheme: TextTheme(
      titleLarge: GoogleFonts.openSans(
        fontWeight: FontWeight.w700,
        fontSize: AppDimensions.d22,
        color: AppColors.shamrock,
        shadows: [
          const Shadow(
            color: AppColors.grayChanteau,
            blurRadius: AppDimensions.d6,
            offset: Offset(0, AppDimensions.d2),
          )
        ],
      ),
      titleMedium: GoogleFonts.openSans(
        fontWeight: FontWeight.w700,
        fontSize: AppDimensions.d20,
        color: AppColors.shamrock,
        shadows: [
          const Shadow(
            color: AppColors.grayChanteau,
            blurRadius: AppDimensions.d6,
            offset: Offset(0, AppDimensions.d2),
          )
        ],
      ),
      titleSmall: GoogleFonts.openSans(
        fontWeight: FontWeight.w700,
        fontSize: AppDimensions.d18,
        color: AppColors.shamrock,
        shadows: [
          const Shadow(
            color: AppColors.grayChanteau,
            blurRadius: AppDimensions.d6,
            offset: Offset(0, AppDimensions.d2),
          )
        ],
      ),
      displayLarge: GoogleFonts.lato(
        fontWeight: FontWeight.w600,
        fontSize: AppDimensions.d18,
        color: Colors.white,
      ),
      displayMedium: GoogleFonts.lato(
        fontWeight: FontWeight.w600,
        fontSize: AppDimensions.d16,
        color: Colors.white,
      ),
      displaySmall: GoogleFonts.lato(
        fontWeight: FontWeight.w600,
        fontSize: AppDimensions.d14,
        color: Colors.white,
      ),
      bodyLarge: GoogleFonts.mulish(
        fontWeight: FontWeight.w600,
        fontSize: AppDimensions.d14,
        letterSpacing: 0.5,
        color: AppColors.shamrock,
        shadows: [
          const Shadow(
            color: AppColors.grayChanteau,
            blurRadius: AppDimensions.d6,
            offset: Offset(0, AppDimensions.d2),
          )
        ],
      ),
      bodyMedium: GoogleFonts.mulish(
        fontWeight: FontWeight.w600,
        fontSize: AppDimensions.d12,
        letterSpacing: 0.5,
        color: AppColors.shamrock,
        shadows: [
          const Shadow(
            color: AppColors.grayChanteau,
            blurRadius: AppDimensions.d6,
            offset: Offset(0, AppDimensions.d2),
          )
        ],
      ),
      bodySmall: GoogleFonts.mulish(
        fontWeight: FontWeight.w600,
        fontSize: AppDimensions.d10,
        letterSpacing: 0.5,
        color: AppColors.shamrock,
        shadows: [
          const Shadow(
            color: AppColors.grayChanteau,
            blurRadius: AppDimensions.d6,
            offset: Offset(0, AppDimensions.d2),
          )
        ],
      ),
      labelMedium: GoogleFonts.montserrat(
        fontWeight: FontWeight.w800,
        fontSize: AppDimensions.d18,
        letterSpacing: 0.5,
        shadows: [
          const Shadow(
            color: AppColors.shark,
            blurRadius: AppDimensions.d4,
          )
        ],
      ),
    ),
    scaffoldBackgroundColor: AppColors.shark,
    dividerTheme: const DividerThemeData(
      color: AppColors.grayChanteau,
      thickness: AppDimensions.d1,
    ),
    progressIndicatorTheme: const ProgressIndicatorThemeData(
      color: AppColors.shamrock,
      circularTrackColor: AppColors.grayChanteau,
    ),
  );

  ThemeData getTheme() => _themeData;
}
