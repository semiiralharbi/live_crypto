import 'package:flutter/material.dart';

/// Color names should be based on http://chir.ag/projects/name-that-color/

class AppColors {
  const AppColors._();

  static const Color shark = Color(0xFF23292f);
  static const Color grayChanteau = Color(0xFFA6A9AA);
  static const Color shamrock = Color(0xFF2ecc71);
  static const Color curiousBlue = Color(0xFF3498db);
}
