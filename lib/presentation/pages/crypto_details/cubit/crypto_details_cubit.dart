import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/entities/trades_entity.dart';
import '../../../../domain/use_case/trades_processor_use_case.dart';

@injectable
class TradesCubit extends Cubit<List<TradesEntity>> {
  TradesCubit(
    this._tradesProcessorUseCase,
  ) : super([]);

  final TradesProcessorUseCase _tradesProcessorUseCase;

  void processTrades(String id) {
    final uniqueTradesStream = _tradesProcessorUseCase(id);
    uniqueTradesStream.listen((uniqueTrades) {
      emit(uniqueTrades);
    });
  }
}
