import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../../../domain/entities/trades_entity.dart';
import '../../../injectable/injectable.dart';
import 'cubit/crypto_details_cubit.dart';

@RoutePage()
class CryptoDetailsPage extends HookWidget {
  const CryptoDetailsPage(this.assetId, {Key? key}) : super(key: key);

  final String assetId;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<TradesCubit>()..processTrades(assetId),
      child: Scaffold(
        body: SafeArea(
          child: BlocBuilder<TradesCubit, List<TradesEntity>>(
            builder: (context, trades) => _Body(trades: trades),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.trades,
  });

  final List<TradesEntity> trades;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const Divider(),
      itemCount: trades.length,
      itemBuilder: (context, index) => ListTile(
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Text(
              trades.elementAt(index).symbolId,
              textAlign: TextAlign.center,
            ),
            const Spacer(),
            Text(
              trades.elementAt(index).timeExchange,
              textAlign: TextAlign.center,
            ),
            const Spacer(),
          ],
        ),
        trailing: Text(
          trades.elementAt(index).takerSide,
          textAlign: TextAlign.center,
        ),
        title: Text(
          '${trades.elementAt(index).price}',
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
