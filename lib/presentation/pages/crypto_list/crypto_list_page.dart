import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../../../domain/entities/asset_icon_entity.dart';
import '../../../injectable/injectable.dart';
import '../../theme/app_colors.dart';
import '../../theme/app_dimensions.dart';
import '../../utils/enums/context_extensions.dart';
import '../../utils/router/app_router.dart';
import '../../widgets/loading_widget.dart';
import 'cubit/crypto_list_cubit.dart';
import 'cubit/crypto_list_state.dart';

@RoutePage()
class CryptoListPage extends StatelessWidget {
  const CryptoListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (context) => getIt<CryptoListCubit>()..init(),
        child: BlocConsumer<CryptoListCubit, CryptoListState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) => state.maybeWhen(
            loading: LoadingWidget.new,
            initial: _ListWidget.new,
            fail: () => ErrorWidget(
              () => context.read<CryptoListCubit>().init(),
            ),
            orElse: () => ErrorWidget(
              () => context.read<CryptoListCubit>().init(),
            ),
          ),
        ),
      );
}

class _ListWidget extends HookWidget {
  const _ListWidget(
    this.entity,
  );

  final List<AssetIconEntity> entity;

  @override
  Widget build(BuildContext context) {
    final searchText = useState('');
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          onChanged: (value) => searchText.value = value,
          style: context.tht.displayMedium,
          cursorColor: AppColors.shamrock,
          textInputAction: TextInputAction.search,
          decoration: InputDecoration(
            hintText: 'Search...',
            hintStyle: context.tht.displayMedium?.copyWith(
              color: AppColors.grayChanteau,
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: AppColors.shamrock),
              borderRadius: BorderRadius.circular(AppDimensions.d16),
            ),
            suffixIconColor: AppColors.shamrock,
            suffixIcon: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.search,
              ),
            ),
            contentPadding: const EdgeInsets.all(AppDimensions.d10),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(AppDimensions.d16),
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: ListView.separated(
          padding: const EdgeInsets.only(top: AppDimensions.d16),
          itemBuilder: (context, index) {
            if (searchText.value.isEmpty ||
                entity[index]
                    .assetId
                    .toLowerCase()
                    .contains(searchText.value.toLowerCase())) {
              return ListTile(
                onTap: () => context.router.push(
                  CryptoDetailsRoute(
                    assetId: entity[index].assetId,
                  ),
                ),
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: AppDimensions.d24,
                ),
                leading: entity[index].iconUrl != null
                    ? Image.network(
                        entity[index].iconUrl!,
                        width: AppDimensions.d44,
                        errorBuilder: (context, _, __) => const Icon(
                          Icons.error_outline,
                          color: Colors.red,
                          size: AppDimensions.d44,
                        ),
                      )
                    : const SizedBox.shrink(),
                title: Text(
                  entity[index].assetId,
                  textAlign: TextAlign.center,
                ),
              );
            } else {
              return const SizedBox.shrink();
            }
          },
          separatorBuilder: (context, index) {
            if (searchText.value.isEmpty ||
                entity[index]
                    .assetId
                    .toLowerCase()
                    .contains(searchText.value.toLowerCase())) {
              return const Divider();
            } else {
              return const SizedBox.shrink();
            }
          },
          itemCount: entity.length,
        ),
      ),
    );
  }
}
