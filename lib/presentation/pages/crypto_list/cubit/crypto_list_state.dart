import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../domain/entities/asset_icon_entity.dart';

part 'crypto_list_state.freezed.dart';

@freezed
class CryptoListState with _$CryptoListState {
  const factory CryptoListState.loading() = _Load;

  const factory CryptoListState.initial(
    List<AssetIconEntity> entity,
  ) = _Initial;

  const factory CryptoListState.fail() = _Fail;
}
