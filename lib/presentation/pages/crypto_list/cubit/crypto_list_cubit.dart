import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/use_case/get_asset_icons_use_case.dart';
import 'crypto_list_state.dart';

@injectable
class CryptoListCubit extends Cubit<CryptoListState> {
  CryptoListCubit(this._assetIconsUseCase)
      : super(const CryptoListState.loading());

  final GetAssetIconsUseCase _assetIconsUseCase;

  Future<void> init() async {
    final result = await _assetIconsUseCase(24);
    result.fold(
      (l) => emit(const CryptoListState.fail()),
      (r) => r.isNotEmpty
          ? emit(CryptoListState.initial(r))
          : emit(const CryptoListState.fail()),
    );
  }
}
