import 'package:auto_route/auto_route.dart';

import 'app_router.dart';

export 'app_router.gr.dart';

@AutoRouterConfig(
  replaceInRouteName: 'Page,Route',
)
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.custom(
        transitionsBuilder: TransitionsBuilders.fadeIn,
        durationInMilliseconds: 400,
      );
  @override
  final List<AutoRoute> routes = [
    AutoRoute(page: CryptoListRoute.page, path: '/'),
    AutoRoute(page: CryptoDetailsRoute.page, path: '/details'),
  ];
}
