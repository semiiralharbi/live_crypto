import 'package:flutter/material.dart';

class ErrorWidget extends StatelessWidget {
  const ErrorWidget({
    Key? key,
    required this.onPressed,
    this.message,
  }) : super(key: key);

  final VoidCallback onPressed;
  final String? message;

  @override
  Widget build(BuildContext context) => Center(
        child: Card(
          child: Column(
            children: [
              Text(message ?? 'Something went wrong'),
              TextButton(
                onPressed: onPressed,
                child: Row(
                  children: const [
                    Icon(
                      Icons.refresh,
                    ),
                    Text('Reload'),
                  ],
                ),
              )
            ],
          ),
        ),
      );
}
