import 'package:injectable/injectable.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../environment_config.dart';

@module
abstract class WsModule {
  @singleton
  WebSocketChannel get channel => WebSocketChannel.connect(
        Uri.parse(EnvironmentConfig.cryptoWS),
      );
}
