import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../environment_config.dart';

@module
abstract class RestApiModule {
  @lazySingleton
  Dio get instance => Dio(
        BaseOptions(
          baseUrl: EnvironmentConfig.cryptoUrl,
          headers: {
            "X-CoinAPI-Key": EnvironmentConfig.cryptoKey,
          },
        ),
      )..interceptors.addAll(
          [
            PrettyDioLogger(
              requestHeader: true,
              requestBody: true,
              responseBody: true,
              responseHeader: false,
              error: true,
              compact: true,
              maxWidth: 120,
            ),
          ],
        );
}
