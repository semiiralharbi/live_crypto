import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import 'injectable/injectable.dart';
import 'presentation/theme/theme_manager.dart';
import 'presentation/utils/router/app_router.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  runApp(const MyApp());
}

@RoutePage()
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: getIt<ThemeManager>().getTheme(),
      routerConfig: getIt<AppRouter>().config(),
    );
  }
}

